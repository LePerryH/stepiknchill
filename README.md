## Naglowek

Paragraf 1

Paragraf 2

Paragraf 3

**pogrubienie** i *kursywa* i ~~skreslenie~~

>greentext
>>?

Lista numeryczna
1. if
2. 20 stopni
3. cieplutko
4. duzo zer

lista nienumeryczna
- i
- love
- stepik
	- stepik n chill
	- ?

Fragment kodu:
```py
temperatura = 21
if temperatura > 20
	print("cieplutko")
```

Tu zagniezdzam kod programu `print("stepik<3")`


![picture/gigachad.jpg](picture/gigachad.jpg)
